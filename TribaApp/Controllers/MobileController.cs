﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TribaApp.Controllers
{
    public class MobileController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult PassLeft()
        {
            return View();
        }

        public ActionResult Training()
        {
            return View();
        }

        public ActionResult MyProgress()
        {
            return View();
        }

        public ActionResult Score()
        {
            return View();
        }

        [HttpGet]
        public JsonResult VerbsTeam()
        {
            return new JsonResult { JsonRequestBehavior = JsonRequestBehavior.AllowGet, Data = new[] { new { id = 1, verb = "Put", correct = true }, new { id = 2, verb = "Cut", correct = true } } };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.WebPages;

namespace TribaApp.Controllers
{   
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var capabilities = this.HttpContext.GetOverriddenBrowser();
            if (capabilities.IsMobileDevice)
                return RedirectToAction("Index", "Mobile");

            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}

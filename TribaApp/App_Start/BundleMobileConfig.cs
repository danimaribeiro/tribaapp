using System.Web;
using System.Web.Optimization;

namespace TribaApp {
    public class BundleMobileConfig {
        public static void RegisterBundles(BundleCollection bundles) {
            bundles.Add(new ScriptBundle("~/bundles/jquerymobile")
                .Include("~/Scripts/jquery.mobile-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include("~/Scripts/knockout-{version}.js"));

            bundles.Add(new StyleBundle("~/Content/mobilecss")
                .Include("~/Content/Site.Mobile.css"));
            
            bundles.Add(new StyleBundle("~/Content/jquerymobilecss")
                .Include("~/Content/jquery.mobile-{version}.css"));
        }
    }
}